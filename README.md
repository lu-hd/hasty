# HASTY
## A GUI Assistant for Liberty University's Helpdesk Remote Support
## Creator: Ian S. Pringle (ipringle2)
## Maintainers: Ian S. Pringle, Joseph Langford

###Prerequisites
Python 3 (https://www.python.org/ftp/python/3.7.0/python-3.7.0.exe)
 - Selenium
 - Requests
 - wx
 - pyyaml

 PowerShell:
 - HD-AD Module (https://bitbucket.org/lu-hd/hd_assist/raw/master/HD-CLI/ad/Modules/HD-AD.psm1)
 - MSO Module

###How to Install
Install all prerequisites. Python 3 can be downloaded form https://python.org
Install Python for all users, and include it in the PATH.
Select Custom Install, select Next, and Select All Users.

Once installed, and added to PATH if it was not done at installation, open cmd with admin rights and then:
`python -m pip install selenium requests wxPython pyyaml`
Next open PowerShell with admin rights.
`cd` into the directory where HD-AD.psm1 is located and then:
`Import-Module ./HD-AD.psm1`

MSO should already be installed for all HD users, additionally it is only needed for the Get APR function, which currently isn't working...

Unzip the Hasty project (https://bitbucket.org/lu-hd/hasty/raw/master/hasty.zip).

Create a `cred.py` file. `cred.py` needs to be edited with usr, pwd, fnpwd, and extension. Make it look like this
```
usr = "username"
pwd = "password"
fnpwd = "finesse password"
extension = "phone number"
```

Create a `config.py` file. `config.py` needs to be edited with the following. Each title and note can be custom to your needs:
```
class QN1():
	title = "Quicknote 1"
	note = "Quicknote 1"

class QN2():
	title = "Quicknote 2"
	note = "Quicknote 2"


class QN3():
	title = "Quicknote 3"
	note = "Quicknote 3"

  class QN4():
      title = "Quicknote 4"
      note = "Quicknote 4"

  class QN5():
      title = "Quicknote 5"
      note = "Quicknote 5"
```

###How to run
Finally, after restarting the computer to source all PATH changes, open a cmd window, `cd` into the directory where Hasty is located and then:
`python hasty.py`

###wxPython Doc
https://docs.wxpython.org/

##Credits:
 * **Jorge Torres** (jtorres78) - Created the original Helper. A number of the AD functions were created
 with assistance of Jorge's work
 * **Joseph Langford** (jlangford7) - Created the Argos and Finesse scrapers
