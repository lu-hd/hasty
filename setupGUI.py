'''
Setup GUI
Version 0.0.1
Authors: Joseph Langford
Release: 10/12/2018

Purpose of this script is to request user credentials in the Remote Support HelpDesk Helper tool and save them into a yaml file for future use
ref: https://www.youtube.com/watch?v=cp1ZeMisTNo
'''
import wx

class HD_Setup( wx.Frame ):

    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, 'Helper Setup', size=(300,200))
        panel = wx.Panel(self)
        submit = wx.Button(panel, label="Submit", pos=(130,10), size=(60,60))
        exit = wx.Button(panel, label="Exit")
        self.Bind(wx.EVT_BUTTON, self.closebutton, exit)
        self.Bind(wx.EVT_CLOSE, self.closewindow)

        filemenu= wx.Menu()

        filemenu.Append(wx.ID_ABOUT, "&About"," Information about this program")
        filemenu.AppendSeparator()
        filemenu.Append(wx.ID_EXIT,"E&xit"," Terminate the program")

       # Creating the menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu,"&File") # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.
        self.Show(True)

        self.Bind(wx.EVT_MENU, self.OnAbout, menuItem)

    def closebutton(self, event):
        self.Close(True)

    def closewindow(self, event):
        self.Destroy()

app = wx.App()
frame = HD_Setup(parent=None, id=-1)
frame.Show()
app.MainLoop()
