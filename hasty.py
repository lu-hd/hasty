
import g

print("""
Hasty v.1.1.0
Authour: Ian S. Pringle
Email: ipringle2@liberty.edu

Initializing...
""")

try:
    import cred

except ModuleNotFoundError:
    g.main()

import wxGUI

if not cred.usr and not cred.pwd and not cred.fnpwd and not cred.extension:
    g.main()
    wxGUI()

else:
    wxGUI()
